with import <nixpkgs> { };

pkgs.mkShell rec {
  venvDir = "./.venv";
  buildInputs = [

    # Install Python & Packages
    (python3.withPackages (ps: with python3Packages; [
      jupyter
      ipython
      ipykernel

      setuptools
      virtualenv
      pip
      pyqt5

      pandas
      numpy
      matplotlib
      scipy
    ]))

    # Install Go (GoNB)
    go

  ];

  shellHook = ''
    # Export Environment Paths
    export GOBIN=$(pwd)/.bin
    export PATH=$PATH:$(pwd)/.bin
    export JUPYTER_DATA_DIR=$(pwd)/.data

    # Upsert Notebook Directory
    mkdir -p ./notebooks

    # Install GoNB
    go install github.com/janpfeifer/gonb@latest
    go install golang.org/x/tools/cmd/goimports@latest
    go install golang.org/x/tools/gopls@latest

    # # fixes libstdc++ issues and libgl.so issues
    # LD_LIBRARY_PATH=${stdenv.cc.cc.lib}/lib/:/run/opengl-driver/lib/
    #
    # # Fix XCB Issues
    # QT_PLUGIN_PATH=${qt5.qtbase}/${qt5.qtbase.qtPluginPrefix}
    # SOURCE_DATE_EPOCH=$(date +%s)
    # QT_XCB_GL_INTEGRATION="none"

    # Upsert Virtual Environment
    if [ -d "${venvDir}" ]; then
      echo "Skipping venv creation, '${venvDir}' already exists"
    else
      echo "Creating new venv environment in path: '${venvDir}'"
      ${python3.interpreter} -m venv "${venvDir}"
    fi

    # Update Python Path
    PYTHONPATH=$PWD/${venvDir}/${python3.sitePackages}/:$PYTHONPATH

    # Activate Virtual Environment
    source "${venvDir}/bin/activate"

    # Update PIP
    python -m pip install --upgrade pip
    python -m ipykernel install --user --name=${venvDir}

    # Install GoNB
    gonb --install
  '';
}
