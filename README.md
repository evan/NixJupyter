# Nix Jupyter Notebooks

The following is a quick way to utilize Nix (`shell.nix`) and `direnv` to automatically spin up a Jupyter environment with both a Python3 and GoNB Kernel.

## Running

```
make run_jupyter
```
